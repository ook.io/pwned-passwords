#!/bin/sh

verbose=0
show_http_status=0

show_help () {
    echo 'Usage: pwned_password.sh [OPTIONS]'
    echo 'Options:'
    echo '      -h|--help|--usage'
    echo '              Show this help'
    echo '      -m|--match'
    echo '              Show matched line'
    echo '      -p|--password '
    echo '              Password to check'
    echo '      -s|--status '
    echo '              Show HTTP status code'
    echo '      -v|--verbose'
    echo '              Increase verbosity (implies -m and -s)'
    echo '              Specifying -v -v will print out the tested pasword'
}

while :; do
    case $1 in
        -h|--help|--usage|-\?)
            show_help
            exit 0
            ;;
        -m|--match)
            show_matched_line=1 
            ;;
        -p|--password)
            if [ "$2" ]; then
                password_to_test=$2 
            fi
            ;;
        -s|--status)
            show_http_status=1 
            ;;
        -v|--verbose)
            verbose=$((verbose + 1)) # Each -v adds 1 to verbosity
            show_matched_line=1
            show_http_status=1
            ;;
        -?*)
            echo 'WARN: Unknown option (ignored): ' "$1" >&2
            ;;
        *)             # Default case: No more options, so break out of the loop
            break
    esac
    shift
done


if [ -p /dev/stdin ]; then
    read -r password_to_test
fi


if [ "$password_to_test" = "" ]; then
   # The stty stuff is used to disable screen echo of the tested password
   stty_orig="$(stty -g)"
   echo 'Enter password to test (typed characters will not be shown in console): '
   stty -echo
   read -r password_to_test
   stty "$stty_orig"
   echo ""


   echo 'Confirm password to test: '
   stty_orig="$(stty -g)"
   stty -echo
   read -r password_confirm
   stty "$stty_orig"

   echo ""

   if [ "$password_confirm" != "$password_to_test" ]; then
       echo 'Passwords do not match. Exiting'
       exit 1
   fi
fi


sha1password="$(printf %s "$password_to_test" | sha1sum | cut -d' ' -f1)"
prefix="$(echo "$sha1password" | cut -c 1-5)" 
suffix="$(echo "$sha1password" | cut -c 6-)"


if [ "$verbose" -gt "0" ]; then
    matched_line_msg="Matched line:"
    matches_msg="Matches     :"
    show_http_status_msg="HTTP Status :"

    if [ "$verbose" -gt "1" ]; then
        echo "Password    :$password_to_test"
    fi

    echo "SHA1 hash   :$sha1password"
    echo "Prefix      :$prefix"
    echo "Suffix      :$suffix"
    echo ".....Calling API.PwnedPasswords.com....."
fi

 # Call Pwned Password API (range)
response="$(curl -w "\nHTTP status :%{http_code}\n" "https://api.pwnedpasswords.com/range/$prefix" 2>/dev/null)"

if [ "$show_http_status" = "1" ]; then
    echo "$show_http_status_msg$(echo "$response" | grep 'HTTP status' | cut -d':' -f2)"
fi

if [ "$show_matched_line" = "1" ]; then
    echo "$matched_line_msg$(echo "$response" | grep -i "$suffix")"
fi

 # always show matches
matches="$(echo "$response" | grep -i "$suffix" | cut -d':' -f2)"

if [ "$matches" = "" ]; then
    matches=0
fi

echo "$matches_msg$matches"

exit 0
