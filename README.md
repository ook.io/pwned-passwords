README.md
=====

What is this all about?
-----
This project provides a command line interface to Troy Hunt's Pwned Passwords API.

Tell me more
-----
The script uses the "Searching by range" method (<https://haveibeenpwned.com/API/v2#SearchingPwnedPasswordsByRange>) to ensure that tested passwords are never transmitted over the wire. The script will only send the first 5 characters of the SHA-1 hash of the tested password and then compares the returned ranges with the full hash to determine how many times the password has been exposed in a breach. This last bit is important: **determine how many times the password has been exposed in a breach**. Just because a tested password returns no matches does not guarantee that it is a good password. 

How do I use it?
-----
Put the `pwned_passwords.sh` script on your `PATH` and call it, or run as a regular script using `./pwned_passwords.sh` while you are in the same directory. The password can be supplied interactively, as a command line parameter, or as piped input. 

~~~~~
$ pwned_passwords.sh 
Enter password to test (typed characters will not be shown in console): 
Confirm password to test: 
16092
~~~~~

Alternatively, a password can be piped in so that it can participate in your awesome one-liner:

~~~~~
$ echo hunter2 | pwned_passwords.sh
16919
~~~~~


Command line options
-----

~~~~~
pwned_passwords.sh -h
Usage: pwned_password.sh [OPTIONS]
Options:
      -h|--help|--usage
              Show this help
      -m|--match
              Show matched line
      -p|--password 
              Password to check
      -s|--status 
              Show HTTP status code
      -v|--verbose
              Increase verbosity (implies -m and -s)
              Specifying -v -v will print out the tested pasword
~~~~~


